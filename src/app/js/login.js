const { DatabaseConnector } = require('../js/databaseConnector.js');
const dbConnector = new DatabaseConnector()
document.getElementById('login').addEventListener('click', (e) => {
    e.preventDefault();
    const username = document.getElementById('username').value
    $queryString = 'Select * from UserAdmin where UserName="' + username + '";';
    let connection = dbConnector.getConnection();
    dbConnector.connectToDB(connection);
    connection.query($queryString, (err, rows, fields) => {
        const label = document.getElementById('login-status');
        const enteredPassword = document.getElementById('password').value;
        if(err) {
            return console.log('Error in query', err);
        } else if (rows.length === 0) {
            label.innerHTML = 'Incorrect Username';
        } else if (rows[0].Password !== enteredPassword) {
            label.innerHTML = 'Incorrect Password';
        } else {
            label.innerHTML = 'Login Successful';
        }
    });
    dbConnector.disconnectFromDB(connection);
}, false);