const mysql = require('mysql');

module.exports.DatabaseConnector = class DatabaseConnector {
    
    getConnection() {
        const connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'stock-management'
        });
        return connection;
    }
    
    connectToDB(connection) {
        connection.connect((err) => {
            if(err) {
                return console.log(err.stack);
            }
        });
    }

    disconnectFromDB(connection){
        connection.end();
    }
}