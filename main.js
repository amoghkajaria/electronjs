const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu} = electron;

let loginWindow;

app.on('ready', function() {
    loginWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
        }
    });

    loginWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'src/app/html/login.html'),
        protocol: 'file:',
        slashes: true
    }));

    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    Menu.setApplicationMenu(mainMenu);
});

const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Quit',
                accelerator: 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    }
]

if(process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push(
        {
            label: 'Devtools',
            submenu: [
                {
                    label: 'Inspect',
                    accelerator: 'Ctrl+I',
                    click(item, focusedWindow) {
                        focusedWindow.toggleDevTools();
                    }
                },
                {
                    role: 'reload'
                }
            ]
        } 
    );
}